""" 
Standardizes the reference file, makes sure that all the relevant sequences are present, 
and creates a file defining the sizes of all the inputs that is required for BAM pileup.
"""

def normalize_references(input_file, out_fa_file, out_region_file, required_seqs):
    from Bio import SeqIO

    fa_handle = SeqIO.parse(input_file, 'fasta')

    # Set of reference sequences that are required for running all the samples
    required_seqs = set(required_seqs)

    seq_dict = {}

    with open(out_fa_file, 'w') as out_handle:

        for record in fa_handle:
            name = str(record.description)

            if name not in required_seqs:  # No need to print unnecessary sequences
                continue

            seq = str(record.seq)
            seq_dict[name] = seq

            print('>' + name, file=out_handle)
            print(seq.upper().replace('U', 'T'), file=out_handle)

    if required_seqs != set(seq_dict.keys()):
        for seq_name in required_seqs:
            if seq_name not in seq_dict.keys():
                print('Error: reference sequence "{}" is missing from the input file {}.'.format(seq_name, input_file))
        raise Exception('Missing reference sequences.')


    with open(out_region_file, 'w') as out_handle:
        for seq_name in seq_dict:
            print(seq_name, 1, len(seq_dict[seq_name]), sep='\t', file=out_handle)


# from sys import argv    
# normalize_references(argv[1], argv[2], argv[3], ['TPP'])

normalize_references(snakemake.input[0], snakemake.output[0], snakemake.output[1], snakemake.config['ref_seq_names'])