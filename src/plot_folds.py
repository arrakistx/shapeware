#!/usr/bin/env python3

"""
Takes SHAPE values and makes plots showing where the reactivity values fall within the structure.

Multiple folding tools are supported and more will be added in the future.

"""


def make_varna_plot(seq_or_file, db_string, shape_values, out_file, title, res_factor=6, masked_bases=[],
                    plot_algorithm='auto'):

    from subprocess import Popen, PIPE
    from os.path import isfile

    stdout, _ = Popen(['which', 'varna'], stdout=PIPE).communicate()
    varna_wrapper_location = stdout.decode().strip()

    if not isfile(varna_wrapper_location):
        raise Exception('VARNA wrapper not found! Please install before using SHAPEware.')

    varna_call = ['varna', '-o', out_file,  '-resolution', str(res_factor),
                  '-title', title, '-titleSize', '6', '-basesStyle2', 'outline=#FF0000']
                  
    if seq_or_file.endswith('.ct'):
        varna_call += ['-i', seq_or_file]

        ct_header = open(seq_or_file).readline().strip().split()
        seq_length = int(ct_header[0])

    else:
        varna_call += ['-sequenceDBN', seq_or_file, '-structureDBN', db_string]
        seq_length = len(db_string)


    if plot_algorithm == 'auto':
        if seq_length >= 300:  # Default to naview for RNAs of length >= 300
            varna_call += ['-algorithm', 'naview']
    
    else:
        varna_call += ['-algorithm', plot_algorithm]


    if shape_values is not None:

        # To avoid errors due to excessively long arguments, trim decimals places in SHAPE values

        shape_values = ';'.join(['%0.2f' % float(_) for _ in shape_values.split(';')])

        varna_call += ['-colorMap', shape_values, '-colorMapStyle', "red", '-colorMapMin', '0',
                       '-colorMapMax', '3', '-colorMapCaption', 'Reactivity']

    if len(masked_bases) > 0:
        varna_call += ['-applyBasesStyle2on', ','.join([str(_ + 1) for _ in masked_bases])]

    try:

        varna_proc = Popen(varna_call, stdout=PIPE, stderr=PIPE)
        varna_stdout, varna_stderr = varna_proc.communicate()

        if varna_proc.returncode != 0:
            print(varna_stdout.decode(), varna_stderr.decode())

        return None

    except:
        print('Error making VARNA plot for: {}. This error can be caused by a very long sequence.'.format(title))
        return None


def fold_vienna(rna_seq, shape_values=None, use_partition=False, blackout_mask=set(), out_file=None, max_span=300):
    """Wrapper function for folding using the RNAfold binary."""


    from tempfile import NamedTemporaryFile
    from subprocess import Popen, PIPE

    fold_results = {}

    rnafold_cmd = ['RNAfold', '--noPS', '--maxBPspan={}'.format(max_span)]
    if use_partition:
        rnafold_cmd.append('-p')

    if shape_values is not None:

        if len(shape_values) != len(rna_seq):
            raise Exception('Received {} SHAPE values for a sequence of length {}.'.format(len(shape_values), len(rna_seq)))

        temp_shape_handle = NamedTemporaryFile('w')

        for pos, value in enumerate(shape_values):
            if value < 0:
                value = 0
            if pos in blackout_mask:
                value = -999

            print(pos + 1, value, file=temp_shape_handle, sep='\t')

        temp_shape_handle.flush()

        rnafold_cmd.append('--shape={}'.format(temp_shape_handle.name))

    p = Popen(rnafold_cmd, stdin=PIPE, stdout=PIPE)
    rnafold_stdout, rnafold_stderr = p.communicate(input=rna_seq.encode())

    fold_output = rnafold_stdout.decode().split('\n')

    fold_results['stdout'] = fold_output
    fold_results['seq'] = fold_output[0].strip()
    fold_results['mfe_db'] = fold_output[1][0:len(rna_seq)]


    if len(fold_output) > 2:
        fold_results['ens_db'] = fold_output[2][0:len(rna_seq)]

    if shape_values is not None:
        temp_shape_handle.close()

    if out_file is not None:
        out_txt_handle = open(out_file, 'w')
        out_txt_handle.write(rnafold_stdout.decode())
        out_txt_handle.close()

    return fold_results 


def fold_mcfold(rna_seq, mcff_dir, bal_mask=None, ub_mask=None, out_file=None):
    """Wrapper function for folding using the MC-Flashfold binary."""

    from subprocess import Popen, PIPE
    from os import path

    fold_results = {}

    mcf_cmd = [path.join(mcff_dir, 'src', 'mcff'), '-s', rna_seq, '-tables', path.join(mcff_dir, 'tables')]

    if bal_mask is not None:
        mcf_cmd += ['-m', bal_mask]
    elif ub_mask is not None:
        mcf_cmd += ['-um', ub_mask]


    p = Popen(mcf_cmd, stdout=PIPE)

    rnafold_stdout, rnafold_stderr = p.communicate()

    with open(out_file, 'wb') as out_txt_handle:
        out_txt_handle.write(rnafold_stdout)

    mcfold_db = rnafold_stdout.decode()[0:len(rna_seq)]

    fold_results['mfe_db'] = mcfold_db

    return fold_results




def fold_shapeknots(rna_seq, shape_values=None, blackout_mask=set(), out_file=None, title='Seq', max_struct=1):
    """Wrapper function for folding using the ShapeKnots binary."""

    from tempfile import NamedTemporaryFile
    from subprocess import Popen, PIPE

    fold_results = {}

    sknots_cmd = ['ShapeKnots', '-m', str(max_struct)]

    if out_file is None:
        out_handle = NamedTemporaryFile()
        out_file = out_handle.name

    if shape_values is not None:

        if len(shape_values) != len(rna_seq):
            raise Exception('Received {} SHAPE values for a sequence of length {}.'.format(len(shape_values), len(rna_seq)))

        temp_shape_handle = NamedTemporaryFile('w')

        for pos, value in enumerate(shape_values):
            if value < 0:
                value = 0
            if pos in blackout_mask:
                value = -999

            print(pos + 1, value, file=temp_shape_handle, sep='\t')

        temp_shape_handle.flush()

        sknots_cmd += ['-sh', temp_shape_handle.name]



    ct_file = out_file + '.ct'
    temp_fa_handle = NamedTemporaryFile('w')

    temp_fa_handle.write('>{}\n{}\n'.format(title, rna_seq))
    temp_fa_handle.flush()

    sknots_cmd += [temp_fa_handle.name, ct_file]

    p = Popen(sknots_cmd, stdout=PIPE)
    rnafold_stdout, rnafold_stderr = p.communicate()

    fold_results['seq'] = rna_seq
    fold_results['mfe_db'] = ct2dot(ct_file)
    fold_results['ct_file'] = ct_file

    if shape_values is not None:
        temp_shape_handle.close()

    return fold_results 


def ct2dot(ct_file):
    """Converts a CT file to dot-bracket notation, with at most two overlapping pseudoknots."""

    import pandas as pd

    df = pd.read_table(ct_file, skiprows=1, header=None, delim_whitespace=True, names=['Pos', 'Base', 'Lpos', 'Rpos', 'Pair', 'Posagain'])

    assert (df.Pos == df.Posagain).all()
    assert (df.Pos == df.Lpos + 1).all()

    num_bases = int(df.Pos.max())

    db_list = ['.' for _ in range(num_bases)]

    bracket_priority = (('(', ')'), ('[', ']'), ('{', '}'))

    already_paired = set()

    for _, row in df.iterrows():
        if row['Pair'] != 0:
            base_from, base_to = sorted([row['Pos'] - 1, row['Pair'] - 1])
            if (base_from, base_to) not in already_paired:

                test_region = db_list[base_from + 1: base_to - 1]

                bracket_to_use = 0
                for i, bracket_type in enumerate(bracket_priority):
                    for bracket in bracket_type:
                        if bracket in test_region:
                            bracket_to_use = i + 1

                            if bracket_to_use >= len(bracket_priority):
                                raise Exception('CT file exceeded number of brackets available for pseudoknots.')

                db_list[base_from] = bracket_priority[bracket_to_use][0]
                db_list[base_to] = bracket_priority[bracket_to_use][1]


            already_paired.add((base_from, base_to))

    return ''.join(db_list)


def plot_folds(agg_shape_file, seq_name, treatment, out_prefix, combined_out_file, config_dict):
    """Plots the folds derived from various tools using VARNA."""

    from subprocess import Popen, PIPE
    import pandas as pd
    from os import path, environ

    combined_out_handle = open(combined_out_file, 'w')

    print('seq_name', 'treatment', 'method', 'mfe_db', file=combined_out_handle, sep='\t')

    shape_df = pd.read_csv(agg_shape_file)
    shape_df = shape_df.loc[(shape_df.Sequence == seq_name) & (shape_df.Treatment == treatment)]

    rna_seq = shape_df.Base.str.cat()
    #shape_values = list([float(_) for _ in shape_df['Mean.Reactivity']])

    shape_values = []
    blackout_mask = set()

    for _, row in shape_df.iterrows():
        if row['Coverage.Pass.Any'] and row['Masked'] == 'No' and not pd.isnull(row['Mean.Reactivity']):
            shape_values.append(row['Mean.Reactivity'])

        elif row['Coverage.Pass.Any'] and row['Masked'] == 'Yes' and not pd.isnull(row['Mean.Reactivity']):
            shape_values.append(row['Mean.Reactivity'])
            blackout_mask.add(int(row['Position']) - 1)
    
        else:
            shape_values.append(0)
            blackout_mask.add(int(row['Position']) - 1)        

    shape_string = ';'.join([str(_) for _ in shape_values])


    if 'rnafold' in config_dict['fold_methods']:

        ### Fold using naive Vienna RNAfold ###

        vienna_naive_fold = fold_vienna(rna_seq, blackout_mask=blackout_mask, out_file=out_prefix + '_naive_vienna-rnafold.txt',
                                        max_span=config_dict['local_fold_constraint'])

        title = '{} (Treatment={}) -- RNAfold (naive)'.format(seq_name, treatment)

        make_varna_plot(rna_seq, vienna_naive_fold['mfe_db'], shape_string, out_prefix + '_naive_vienna-rnafold.png',
                        title, masked_bases=blackout_mask)
        print(seq_name, treatment, 'RNAfold (naive)', vienna_naive_fold['mfe_db'], file=combined_out_handle, sep='\t')


        ### Fold using constrained Vienna RNAfold ###

        vienna_shape_fold = fold_vienna(rna_seq, blackout_mask=blackout_mask, shape_values=shape_values,
                                        out_file=out_prefix + '_constrained_vienna-rnafold.txt', 
                                        max_span=config_dict['local_fold_constraint'])

        title = '{} (Treatment={}) -- RNAfold (SHAPE-constrained)'.format(seq_name, treatment)
        make_varna_plot(rna_seq, vienna_shape_fold['mfe_db'], shape_string, out_prefix + '_constrained_vienna-rnafold.png',
                        title, masked_bases=blackout_mask)
        print(seq_name, treatment, 'RNAfold (constrained)', vienna_shape_fold['mfe_db'], file=combined_out_handle, sep='\t')


    if 'shapeknots' in config_dict['fold_methods']:

        ### Fold using ShapeKnots from RNAstructure ###

        # Test for presence of ShapeKnots executable

        stdout, _ = Popen(['which', 'ShapeKnots'], stdout=PIPE).communicate()
        sk_bin_location = stdout.decode().strip()

        if path.isfile(sk_bin_location):

            # Try to find ShapeKnots tables directory if not configured as an environmental variable

            if 'DATAPATH' not in environ:
                environ['DATAPATH'] = ''

            if not path.isdir(environ['DATAPATH']):
                relative_dir = path.expanduser(path.dirname(sk_bin_location))
                tables_dir = path.join('/'.join(relative_dir.split('/')[:-1]), 'share', 'rnastructure', 'data_tables')
                environ['DATAPATH'] = tables_dir

            if path.isdir(environ['DATAPATH']):

                # Naive folding

                title = '{}_(Treatment={})__ShapeKnots(naive)'.format(seq_name, treatment)
                sknots_naive_fold = fold_shapeknots(rna_seq, blackout_mask=blackout_mask, 
                                                    out_file=out_prefix + '_naive_shapeknots.txt', title=title)

                
                make_varna_plot(sknots_naive_fold['ct_file'], None, shape_string, out_prefix + '_naive_shapeknots.png',
                                title, masked_bases=blackout_mask)

                print(seq_name, treatment, 'ShapeKnots (naive)', sknots_naive_fold['mfe_db'], file=combined_out_handle, sep='\t')

                # With SHAPE

                title = '{}_(Treatment={})__ShapeKnots(constrained)'.format(seq_name, treatment)
                sknots_constrained_fold = fold_shapeknots(rna_seq, shape_values=shape_values, blackout_mask=blackout_mask, 
                                                          out_file=out_prefix + '_constrained_shapeknots.txt', title=title)

                make_varna_plot(sknots_constrained_fold['ct_file'], None, shape_string, 
                                out_prefix + '_constrained_shapeknots.png',
                                title, masked_bases=blackout_mask)

                print(seq_name, treatment, 'ShapeKnots (constrained)', 
                      sknots_constrained_fold['mfe_db'], file=combined_out_handle, sep='\t')

        else:
            print('ShapeKnots executable not found. Skipping ShapeKnots folding.')


    combined_out_handle.close()


if __name__ == '__main__':
    import json
    from os import path, makedirs

    out_dir = path.join('plots', 'folds')
    if not path.isdir(out_dir):
        makedirs(out_dir, exist_ok=True)

    out_prefix = path.join(out_dir, '{}_{}_{}'.format(snakemake.wildcards['seq'], snakemake.wildcards['treatment'], snakemake.wildcards['method']))

    plot_folds(snakemake.input[0], snakemake.wildcards['seq'], snakemake.wildcards['treatment'], out_prefix,
               snakemake.output[0], snakemake.config)

