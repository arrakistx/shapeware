""" Takes a CT file as input and outputs dot bracket notation. 

For now, it handles at most two overlapping pseudoknots. This seems limited by the number of brackets, but I will expand."""

import pandas as pd
from sys import argv

df = pd.read_table(argv[1], skiprows=1, header=None, delim_whitespace=True, names=['Pos', 'Base', 'Lpos', 'Rpos', 'Pair', 'Posagain'])


assert (df.Pos == df.Posagain).all()
assert (df.Pos == df.Lpos + 1).all()

num_bases = int(df.Pos.max())

db_list = ['.' for _ in range(num_bases)]

bracket_priority = (('(', ')'), ('[', ']'), ('{', '}'))

already_paired = set()

for _, row in df.iterrows():
    if row['Pair'] != 0:
        base_from, base_to = sorted([row['Pos'] - 1, row['Pair'] - 1])
        if (base_from, base_to) not in already_paired:
            print(base_from, base_to)

            test_region = db_list[base_from + 1: base_to - 1]
            print(test_region)

            bracket_to_use = 0
            for i, bracket_type in enumerate(bracket_priority):
                for bracket in bracket_type:
                    if bracket in test_region:
                        bracket_to_use = i + 1

                        if bracket_to_use >= len(bracket_priority):
                            raise Exception('CT file exceeded number of brackets available for pseudoknots.')

            db_list[base_from] = bracket_priority[bracket_to_use][0]
            db_list[base_to] = bracket_priority[bracket_to_use][1]

            print(''.join(db_list))

        already_paired.add((base_from, base_to))




