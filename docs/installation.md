![Logo](../img/SHAPEware-logo.png)

***
# How to install SHAPEware dependencies

## Docker

In-depth knowledge of Docker and containers is not needed to run Shapeware.

Docker as an open-source containerization software that enables apps to be shared across Mac, Windows, and Linux platforms.

You can find installation instructions [here](https://docs.docker.com/install/). We recommend command-line installation via package managers, but any Docker system should work. 

Test your installation of Docker using a lightweight image such as busybox:

    docker pull busybox
    docker run busybox echo "Docker works"

***

## Git

You most likely already have Git, but if you don't, you can download it from [here](https://git-scm.com/downloads).

On MacOS, it is also part of [Xcode](https://developer.apple.com/xcode/).