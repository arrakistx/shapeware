#!/usr/bin/env python3

""" Initializes the a run of the SHAPEware SHAPE-MaP pipeline.

Designed for Python >=3.5. Backward compatibility is untested.

Currently, the only required input is the directory that the raw data and metadata files. The following files should be 
placed in this directory, but the filenames can also be overwritten with specific arguments.

./raw_data/  <-- Contains all the raw FASTQ files
./input_sheet.csv  <-- Spreadsheet that describes how the experiments are grouped
./ref_seqs.fa  <-- FASTA file that contains all the reference sequences
./ref_masks.fa  <-- Contains reference sequence masks, with masked nucleotides replaced with "x" or "X"
(This can just be a copy of ref_seqs.fa if there is no mask)

Optional files:

./ref_structures.dot  <-- Contains known 2D structures, using FASTA format except the third line is a dot-bracket string

See the example_data/ folder within the SHAPEware directory for an example dataset.

"""

from __future__ import print_function, division

__version__ = '0.2.2'
__author__ = 'Luis Barrera'
__license__ = 'GPLv3'

# Load standard modules

import argparse
import re
from glob import glob
from os import path, makedirs, symlink, environ
from multiprocessing import cpu_count
from subprocess import Popen, PIPE
from time import sleep

# Load non-standard modules

try:
    import json
    import pandas as pd

    from Bio import SeqIO
    from snakemake import snakemake

except ModuleNotFoundError as e:
    print(e)
    print('===========')
    print('Failed to find a required module. Did you forget to activate the "shapeware" environment?')
    print('Try:\n source activate shapeware')
    exit()
    

# Check for input file presence and validity

pipeline_dir = path.dirname(path.realpath(__file__))

# Check that R is installed and available

r_proc = Popen(['which', 'Rscript'], stdout=PIPE,stderr=PIPE)
if r_proc.wait() != 0:
    raise Exception('Rscript binary not found. Please install before using.')

# Check that required R libraries are also installed

r_proc = Popen(['Rscript', path.join(pipeline_dir, 'src', 'check_packages.R')])
if r_proc.wait() != 0:
    raise Exception('Missing R packages found. Automated installation failed. Please install them manually. \
                     (This can be due to a fresh installation of R. Install any package manually and try again.)')

# Check that SHAPE is running in correct environment

if 'CONDA_DEFAULT_ENV' in environ:
    if environ['CONDA_DEFAULT_ENV'] != 'shapeware':
        print('Warning: SHAPEware is running in Conda environment "{}"'.format(environ['CONDA_DEFAULT_ENV']))
        print('We recommend creating a dedicated SHAPEware environment called "shapeware".')
        print('===========')
        sleep(2)

###################
# Parse arguments #
###################

parser = argparse.ArgumentParser(description="""SHAPEware is a software package for analysis of RNA chemical probing 
                                                data. This module analyzes SHAPE-MaP data.
                                                For more information, visit:
                                                https://bitbucket.org/arrakistx/shapeware """)

parser.add_argument('input_dir', help='Working directory where input files are found and output files will be created')
parser.add_argument('--input_sheet', help='Input spreadsheet, if not found in standard location')
parser.add_argument('--raw_data_dir', help='Directory containing FASTQ files')
parser.add_argument('--ref_fa', help='FASTA file containing reference sequences')
parser.add_argument('--mask_file', help='FASTA file describing masked positions in reference sequences')
parser.add_argument('--struct_file', help='File containing known 2D structures (see README for details)')
parser.add_argument('--num_cores', help='Number of cores to use (default: all available)', type=int, default=cpu_count())
parser.add_argument('--align_method', help='Alignment method (available: bwa-mem, bowtie2)', default='bwa-mem')
parser.add_argument('--min_coverage', help='Minimum read coverage to calculate reactivities at a given position', 
                    type=int, default=1000)
parser.add_argument('--max_mut_rate', help="""Positions with mutation rates greater than this fraction 
                                            will be filtered out (0-1).""", 
                    type=float, default=0.2)
parser.add_argument('--local_fold_constraint', help="""For local folding predictions, the maximum distance to allow for 
                    base pairing (nt).""", type=int, default=300)
parser.add_argument('--max_indel_length', help="""Maximum length of indels to consider for reactivity calculations.
                    Indels with length > this value will be discarded.""", type=int, default=10)
parser.add_argument('--min_read_length', help="""Minimum read length after trimming by base quality.""", type=int, 
                    default=30)
parser.add_argument('--use_only_pairs', help='Only use read pairs where both mates survive trimming and QC.',
                    action='store_true')
parser.add_argument('--skip_insertions', help='Do not use insertions when calculating reactivity profiles.',
                    action='store_true')
parser.add_argument('--skip_deletions', help='Do not use deletions when calculating reactivity profiles.',
                    action='store_true')
parser.add_argument('--fold_methods', help='Specify the methods used for folding, as a comma separated list.',
                    default='rnafold,shapeknots')
parser.add_argument('--overwrite',
                    help='Overwrite final output files even if precurors unchanged.', action='store_true')

args = parser.parse_args()

if not path.isdir(args.input_dir):
    raise Exception('Working directory {} not found.'.format(args.input_dir))



if args.fold_methods != '' and args.fold_methods != 'none':

    avail_methods = ['rnafold', 'shapeknots']

    fold_methods = [_.lower() for _ in args.fold_methods.split(',')]

    for method in fold_methods:
        if method not in avail_methods:
            raise Exception('Method "{}" for folding predictions not recognized.'.format(method))

else:
    fold_methods = []


# Assign default values based on relative path when not explicitly provided 
# (This is done after initial parsing because they depend on args.input_dir)

default_args = [('input_sheet', 'input_sheet.csv'), ('ref_fa', 'ref_seqs.fa'),
                ('mask_file', 'ref_masks.fa'), ('raw_data_dir', 'raw_data/'),
                ('struct_file', 'ref_structures.dot')] 

for arg_name, default_value in default_args:
    if not getattr(args, arg_name):
        setattr(args, arg_name, path.join(args.input_dir, default_value))


# Perform basic validation on files

if not path.isfile(args.input_sheet):
    raise Exception('Input spreadsheet not found at: {}'.format(args.input_sheet))

if args.input_sheet.endswith('.xlsx') or args.input_sheet.endswith('.xls'):
    sample_df = pd.read_excel(args.input_sheet)

elif args.input_sheet.endswith('.csv'):
    sample_df = pd.read_csv(args.input_sheet)

if len(sample_df) == 0:
    raise Exception('Input spreadsheet is empty or corrupted.')

if not path.isfile(args.ref_fa):
    raise Exception('Reference sequence file not found at: {}'.format(args.ref_fa))

if not path.isfile(args.mask_file):
    raise Exception('Reference mask file not found at: {}. \
                     This can just be a copy of the reference sequence file'.format(args.mask_file))

if args.max_mut_rate < 0 or args.max_mut_rate >= 1:
    raise Exception('--max_mut_rate must be a value between 0 and 1.')



###############################
# Verify presence of raw data #
###############################

# Read reference sequences  

ref_seqs_from_fasta = SeqIO.to_dict(SeqIO.parse(args.ref_fa, 'fasta'))

if len(ref_seqs_from_fasta) == 0:
    raise Exception('Provided reference FASTA contains no sequences.')


# Identify the raw FASTQ files that match each sample and match the samples to their reference sequence
# More generally, this gathers metadata pertaining to specific samples, such as the presence of a treatment/ligand 
# and associated reference sequences.

# In addition, we check that samples are consistently associated with reference sequences and treatments.


def parse_ref_seqs(ref_seq_string, ref_seqs_from_fasta):
    """Parses the "Reference sequence" field in the input spreadsheet."""

    if ref_seq_string == '*':
        ref_list = list(ref_seqs_from_fasta.keys())

    elif ',' in ref_seq_string:
        ref_list = ref_seq_string.split(',')

    elif ';' in ref_seq_string:
        ref_list = ref_seq_string.split(';')
    else:
        ref_list = [ref_seq_string]

    for seq_name in ref_list:
        if seq_name not in ref_seqs_from_fasta:
            raise Exception('Sequence {} provided in input spreadsheet is not in reference FASTA file.'.format(seq_name))

    return ref_list

fastq_dict, ref_seq_dict, treatment_dict = {}, {}, {}
trio_keys = []

for _, row in sample_df.iterrows():
    sample_trio = (row['Sample with SHAPE reagent'], row['Sample without SHAPE reagent'], row['Denatured sample'])

    for sample_name in sample_trio:
        if '_' in sample_name:
            raise Exception('For now, SHAPEware does not support underscores in sample names.\
                Please replace them in sample {}.'.format(sample_name))
        if sample_name != '*':
            if not re.match(r'^[a-zA-Z0-9][A-Za-z0-9-]*$', sample_name):
                print('-- Sample names should only include alphanumeric characters and dashes, or a single "*". --')
                raise Exception('Problematic sample name: {}'.format(sample_name))


    trio_key = '_'.join(sample_trio)
    trio_keys.append(trio_key)

    for sample_name in sample_trio:

        if sample_name not in ref_seq_dict:
            ref_seq_dict[sample_name] = parse_ref_seqs(row['Reference sequence'], ref_seqs_from_fasta)
            
            if 'Treatment' in row:
                treatment_var = 'Treatment'
            elif 'Ligand' in row:
                treatment_var = 'Ligand'
            else:
                raise Exception('Did not find a column specifying treatment or ligand')

            treatment_dict[sample_name] = row[treatment_var]

        else:
            assert ref_seq_dict[sample_name] == parse_ref_seqs(row['Reference sequence'], ref_seqs_from_fasta)
            if treatment_dict[sample_name] != row[treatment_var]:
                print('Warning: sample {} had been associated with treatment {} but is now being associated with {}.'\
                      .format(sample_name, treatment_dict[sample_name], row[treatment_var]))

        all_fastq_files = sorted(glob(path.join(args.raw_data_dir, '**/*{}_*.fastq.gz'.format(sample_name)), recursive=True))

        # Try to match by sample name
        matching_fastq_files = [_ for _ in all_fastq_files if re.search('[/_]' + sample_name + '_S[0-9]{1,4}_', _)]

        # If that fails, try to use the sample name as a potential sample number
        if len(matching_fastq_files) == 0:
            matching_fastq_files = [_ for _ in all_fastq_files if re.search('_' + sample_name + '_', _)]

        if len(matching_fastq_files) != 2:
            print(' '.join(matching_fastq_files))
            print(row)
            raise Exception('Wrong number of matching FASTQ files for sample.')

        assert '_R1_' in matching_fastq_files[0]
        assert '_R2_' in matching_fastq_files[1]
        assert matching_fastq_files[0].replace('_R1_', '_R0_') == matching_fastq_files[1].replace('_R2_', '_R0_')

        fastq_dict[sample_name] = tuple(matching_fastq_files)

sample_names = list(fastq_dict.keys())
all_ref_seq_names = list(set([_ for sample_name in ref_seq_dict for _ in ref_seq_dict[sample_name]]))

config_dict = {'ref_seq_dict': json.dumps(ref_seq_dict), 'ref_seq_names': all_ref_seq_names,
               'treatment_dict': json.dumps(treatment_dict), 'sample_trios': trio_keys, 'sample_names':sample_names,
               'version': __version__, 'min_coverage': args.min_coverage, 'max_mut_rate': args.max_mut_rate,
               'local_fold_constraint': args.local_fold_constraint, 'max_indel_length': args.max_indel_length,
               'min_read_length': args.min_read_length, 'fold_methods': fold_methods,
               'skip_insertions': args.skip_insertions, 'skip_deletions': args.skip_deletions}

if args.use_only_pairs:
    config_dict['read_files_to_use'] = ['R0']

else:
    config_dict['read_files_to_use'] = ['R0', 'R1', 'R2']    

# Create a folder with symlinks to FASTQ files in a way that they are now named consistently with sample names

sanitized_fastq_dir = path.join(args.raw_data_dir, 'sanitized')

if not path.isdir(sanitized_fastq_dir):
    makedirs(sanitized_fastq_dir)

for sample_name in fastq_dict:
    r1_file, r2_file = fastq_dict[sample_name]
    
    new_r1_file = path.join(sanitized_fastq_dir, '{}_R1.fastq.gz'.format(sample_name))
    new_r2_file = path.join(sanitized_fastq_dir, '{}_R2.fastq.gz'.format(sample_name))

    if not path.exists(new_r1_file):
        symlink(path.relpath(r1_file, sanitized_fastq_dir), new_r1_file)

    if not path.exists(new_r2_file):
        symlink(path.relpath(r2_file, sanitized_fastq_dir), new_r2_file)

    fastq_dict[sample_name] = (new_r1_file, new_r2_file)


# Read blackout masks, if available

mask_dict = {}
if path.exists(args.mask_file):
    mask_fa_dict = SeqIO.to_dict(SeqIO.parse(args.mask_file, 'fasta'))

    for seq_name in mask_fa_dict:
        masked_pos_list = [i  for i, _ in enumerate(mask_fa_dict[seq_name].seq) if _ in ['x', 'X']]

        # Because R is weird about empty vectors, add a dummy mask with position -1
        if len(masked_pos_list) == 0:
            masked_pos_list = [-1]

        mask_dict[seq_name] = masked_pos_list

config_dict['mask_dict'] = json.dumps(mask_dict)

# Read known structures, if available

ref_struct_dict = {}

if path.exists(args.struct_file):

    # Read provided sequences and make sure they match the reference sequencesj
    validation_ref_seqs = SeqIO.to_dict(SeqIO.parse(args.ref_fa, 'fasta'))

    for line_index, line in enumerate(open(args.struct_file)):
        if line[0] == '>':
            seq_name = line[1:].strip()
            seq_index, dotb_index = line_index + 1, line_index + 2

        if line_index == seq_index:
            assert line.strip().upper().replace('T', 'U') == \
                str(validation_ref_seqs[seq_name].seq).upper().replace('T', 'U')

        if line_index == dotb_index:
            ref_struct_dict[seq_name] = line.strip()


config_dict['ref_struct_dict'] = json.dumps(ref_struct_dict)


################################
# Run analysis using Snakemake #
################################

# Define per-sample targets and generate prerequisite files using Snakemake

per_sample_targets = []

for _, row in sample_df.iterrows():
    sample_trio = (row['Sample with SHAPE reagent'], row['Sample without SHAPE reagent'], row['Denatured sample'])
    
    per_sample_targets.append("plots/reactivity_values/{}_{}_{}_{}_reactivity_values.pdf".format(*sample_trio, args.align_method))
    per_sample_targets.append("plots/mut_identities/{}_{}_{}_{}_mutations.pdf".format(*sample_trio, args.align_method))
    per_sample_targets.append("plots/mutation_rates/{}_{}_{}_{}_mutation_rate.pdf".format(*sample_trio, args.align_method))

# Define aggregate output final targets 

final_targets = ['plots/agg_reactivity_boxplot_{}.pdf', 'plots/agg_reactivity_line_graph_{}.pdf',
                 'output/agg_reactivity_values_{}.csv', 'plots/agg_coverage_plot_{}.pdf',
                 'output/agg_coverage_{}.csv']

final_targets = [_.format(args.align_method) for _ in final_targets]


plot_targets = []

if len(fold_methods) > 0:

    for _, row in sample_df[['Reference sequence', treatment_var]].drop_duplicates().iterrows():

        seqs_to_use = parse_ref_seqs(row['Reference sequence'], ref_seqs_from_fasta)

        for ref_seq_name in seqs_to_use:

            plot_targets.append('output/folds/{}_{}_{}_2d_fold_predictions.txt'.format(
                                ref_seq_name, row[treatment_var], args.align_method))

# Run 2D folding benchmark if a structure file was provided that matches sequences in the dataset

benchmark_targets = []

if len(ref_struct_dict) > 0 and len(fold_methods) > 0:

    for seq_name in ref_struct_dict:
        sub_df = sample_df.loc[(sample_df['Reference sequence'] == seq_name) |
                               (sample_df['Reference sequence'].str.contains(',{}|{}$'.format(seq_name, seq_name))) | 
                               (sample_df['Reference sequence'] == '*')]
       
        if len(sub_df) > 0:

            for _, row in sub_df[['Reference sequence', treatment_var]].drop_duplicates().iterrows():
                benchmark_targets.append('output/benchmarks/{}_{}_{}_2d_fold_benchmark.txt'.format(seq_name,
                                                                                                   row[treatment_var],
                                                                                                   args.align_method))
                benchmark_targets.append('plots/benchmarks/{}_{}_{}_2d_fold_benchmark.pdf'.format(seq_name,
                                                                                                  row[treatment_var],
                                                                                                  args.align_method))



all_targets = per_sample_targets + final_targets + plot_targets + benchmark_targets


snakemake(path.join(pipeline_dir, 'src/Snakefile'), targets=all_targets, forcetargets=args.overwrite,
          workdir=args.input_dir, cores=args.num_cores, dryrun=False, config=config_dict)

# Write parameters to a JSON dump file

with open(path.join(args.input_dir, 'params.json'), 'w') as out_handle:
    json.dump(config_dict, out_handle)


print('======================')
