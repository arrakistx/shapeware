#!/usr/bin/env bash

# Helper script to install dependencies for SHAPEware

#build docker image, which includes Conda
docker build -t shapeware .

echo -e "Shapeware can be run using the following command, which we recommend aliasing for convenience: \n 'docker run -u $(id -u ${USER}):$(id -g ${USER}) -v `pwd`:/home shapeware'"

if grep -q 'alias shapeware' ~/.bash_profile; then
	echo 'shapeware alias already in .bash_profile'
else
	echo "Do you want to add an alias to call shapeware? (y/n)"
	read alias_option
	if [ $alias_option == 'y' ] || [ $alias_option == 'Y' ]; then		
		echo 'Adding shapeware alias to .bash_profile'
		echo -e '\n#added by shapeware installer:' >> ~/.bash_profile
		#note - must used single quotes around alias to ensure pwd is evaluated on each run. Backticks prevent evaluation during write to .bash_profile 
		echo "alias shapeware='docker run -u $(id -u ${USER}):$(id -g ${USER}) -v \`pwd\`:/home shapeware'" >> ~/.bash_profile
		source ~/.bash_profile
	fi
fi

echo "Do you want to download the example files? (y/n)"
read dl_option

if [ $dl_option == 'y' ] || [ $dl_option == 'Y' ]; then
	curl_path=$(command -v curl)
	wget_path=$(command -v wget)
	if [ ! -f $curl_path ]; then
		echo "curl binary not found. curl or wget is required to download files."
		
	elif [ ! -f $wget_path ]; then
		echo "wget binary not found. curl or wget is required to download files."
		exit 1
	else
		example_data/download_files.sh
	fi
fi

echo -e "\nYou should now be able to test SHAPEware with the following commands:"
echo -e "shapeware example_data"
echo -e "\nIf you did not set a shapeware alias, run:"
echo -e "docker run -u $(id -u ${USER}):$(id -g ${USER}) -v `pwd`:/home shapeware example_data\n"