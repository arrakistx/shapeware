#base docker image: ubuntu
FROM continuumio/miniconda:4.7.12
MAINTAINER Lee Vandivier lvandivier@arrakistx.com

#Set the working directory
USER root

#Install shapeware dependencies via conda
RUN conda create -y --name shapeware python=3.6
RUN conda install -y --name shapeware -c lb_arrakistx \
 varna=3.93
RUN conda install -y --name shapeware -c conda-forge \
 pigz=2.3.4 \
 numpy=1.18.1 \
 pandas=1.0.1 \
 seaborn=0.10.0 \
 matplotlib=3.1.3
RUN conda install -y --name shapeware -c bioconda \
 bam-readcount=0.8 \
 bedtools=2.29.2 \
 samtools=1.6 \
 pear=0.9.6 \
 umi_tools=1.0.1 \
 biopython=1.76 \
 bowtie2=2.3.5.1 \
 pysam=0.15.3 \
 trimmomatic=0.39 \
 cutadapt=1.18 \
 bwa=0.7.17 \
 viennarna=2.4.14 \
 rnastructure=6.1
RUN conda install -y --name shapeware -c r \
 r-base=3.4.3 \
 r-ggplot2=2.2.1 \
 r-dplyr=0.7.4 \
 r-jsonlite=1.5 \
 r-plyr=1.8.4 \
 r-reshape2=1.4.3 \
 r-stringr=1.2.0
RUN conda install -y --name shapeware -c conda-forge -c bioconda \
 snakemake=5.3.0

# Make conda env accessible
RUN echo "source activate shapeware" > ~/.bashrc
ENV PATH /opt/conda/envs/shapeware/bin:$PATH

# Copy shapeware src code
COPY ./analyze_shape-map.py /app/analyze_shape-map.py
COPY ./src/ app/src

WORKDIR /home

ENTRYPOINT [ "python", "/app/analyze_shape-map.py" ]
