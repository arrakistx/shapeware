#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir $DIR/raw_data


while IFS= read -r file
do
	curl $file -o $DIR/raw_data/$(basename $file)
	if [ $? != 0 ]; then
		wget $file -P $DIR/raw_data/
	fi
	
done < $DIR/files_to_download.txt